
PRAGMA foreign_keys = ON;

CREATE TABLE Places (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,
					   name TEXT NOT NULL  check(typeof(name) = 'text'),
					   address TEXT check(typeof(address) = 'text'),
					   web_address TEXT check(typeof(web_address) = 'text'));

CREATE TABLE Menus (place_id INTEGER NOT NULL,
					  menu_id INTEGER NOT NULL,
					  name TEXT NOT NULL  check(typeof(name) = 'text'),
					  PRIMARY KEY (place_id, menu_id),
					  FOREIGN KEY(place_id) REFERENCES Places(id));

CREATE TABLE DailyMenus (place_id INTEGER NOT NULL,
						   menu_id INTEGER NOT NULL,
						   date DATETIME NOT NULL,
						   item TEXT NOT NULL  check(typeof(item) = 'text'),
						   FOREIGN KEY(place_id, menu_id) REFERENCES Menus(place_id, menu_id));

INSERT INTO Places(name, address, web_address) VALUES("La Cina", "Cluj-Napoca", "www.la-cina.ro");
INSERT INTO Menus(place_id, menu_id, name) VALUES(1, 1, "Meniul zilei 1");
INSERT INTO Menus(place_id, menu_id, name) VALUES(1, 2, "Meniul zilei 2");

INSERT INTO DailyMenus(place_id, menu_id, date, item) VALUES(1, 1, "2016-06-22", "Supa de taietei");
INSERT INTO DailyMenus(place_id, menu_id, date, item) VALUES(1, 1, "2016-06-22", "Cartofi prajiti");
INSERT INTO DailyMenus(place_id, menu_id, date, item) VALUES(1, 1, "2016-06-22", "Salata");
INSERT INTO DailyMenus(place_id, menu_id, date, item) VALUES(1, 1, "2016-06-22", "Desert");
