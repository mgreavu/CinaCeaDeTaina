package com.gorunului.mig.cinaceadetaina;

import java.util.Vector;
import android.util.Log;

/**
 * Created by mig on 6/20/2016.
 */
public class LaCinaParser extends LunchParser
{
    private static final String TOKEN_MENU_DATA_BEGIN = "LUNI-VINERI INTRE 12:00-15:00";
    private static final String TOKEN_MENU_OPTION_NAME_BEGIN = "<font style=\"font-family:Lato;color:#3b3b3b;font-weight:700;font-size:28px;\">";
    private static final String TOKEN_MENU_OPTION_NAME_END = "</font>";
    private static final String TOKEN_MENU_OPTION_ITEM_BEGIN = "&bull; ";
    private static final String TOKEN_MENU_OPTION_ITEM_END = "<br>";
    private static final String TOKEN_MENU_OPTION_PRICE = "Pret : 23,00 ron";

    private static final String TAG = "LaCina";

    public LaCinaParser(String data)
    {
        super(data);
    }

    public boolean Parse()
    {
        int tokenFirstIdx = m_data.indexOf(TOKEN_MENU_DATA_BEGIN);
        int tokenLastIdx = m_data.lastIndexOf(TOKEN_MENU_OPTION_PRICE);   // last!
        if (tokenFirstIdx == -1 || tokenLastIdx == -1)
        {
            Log.w(TAG, "Menu data section not found");
            return false;
        }

        // extract the chunk of the web page where the menu data is located
        String allMenuOptions = m_data.substring(tokenFirstIdx, tokenLastIdx + TOKEN_MENU_OPTION_PRICE.length());

        // first tokenization level, by TOKEN_MENU_OPTION_PRICE substring
        Vector<Integer> veTokenPrice = new Vector<>(16);
        int tokenPriceIdx = 0;
        while(tokenPriceIdx != -1)
        {
            veTokenPrice.add(tokenPriceIdx);
            tokenPriceIdx = allMenuOptions.indexOf(TOKEN_MENU_OPTION_PRICE, tokenPriceIdx + TOKEN_MENU_OPTION_PRICE.length());
        }

        if (veTokenPrice.size() < 2)
        {
            Log.e(TAG, "Invalid menu data section");
            return false;
        }

        StringBuffer resBuff = new StringBuffer();
        for (int i = 0; i < veTokenPrice.size() - 1; i++)
        {
            String menuOption = allMenuOptions.substring(veTokenPrice.elementAt(i), veTokenPrice.elementAt(i + 1));

            // fetch menu option name
            tokenFirstIdx = menuOption.indexOf(TOKEN_MENU_OPTION_NAME_BEGIN);
            tokenLastIdx = menuOption.indexOf(TOKEN_MENU_OPTION_NAME_END, tokenFirstIdx + TOKEN_MENU_OPTION_NAME_BEGIN.length());
            if (tokenFirstIdx != -1 && tokenLastIdx != -1)
            {
                resBuff.append(menuOption.substring(tokenFirstIdx + TOKEN_MENU_OPTION_NAME_BEGIN.length(), tokenLastIdx));
                resBuff.append(System.lineSeparator());
            }

            // extract the individual items of a menu option
            tokenFirstIdx = 0;
            while(true)
            {
                tokenFirstIdx = menuOption.indexOf(TOKEN_MENU_OPTION_ITEM_BEGIN, tokenFirstIdx);
                tokenLastIdx = menuOption.indexOf(TOKEN_MENU_OPTION_ITEM_END, tokenFirstIdx + TOKEN_MENU_OPTION_ITEM_BEGIN.length());
                if (tokenFirstIdx == -1 || tokenLastIdx == -1)
                {
                    break;
                }

                String menuOptionItem = menuOption.substring(tokenFirstIdx + TOKEN_MENU_OPTION_ITEM_BEGIN.length(), tokenLastIdx);
                menuOptionItem.trim();
                resBuff.append(menuOptionItem);
                resBuff.append(System.lineSeparator());
                tokenFirstIdx = tokenLastIdx + TOKEN_MENU_OPTION_ITEM_END.length();
            }
            resBuff.append("*****");
            resBuff.append(System.lineSeparator());
        }

        m_result = resBuff.toString();
        Log.i(TAG, "Parsing completed");
        return true;
    } // Parse()
}
