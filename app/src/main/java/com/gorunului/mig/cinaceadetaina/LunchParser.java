package com.gorunului.mig.cinaceadetaina;

/**
 * Created by mig on 6/20/2016.
 */
public abstract class LunchParser
{
    public LunchParser(String data)
    {
        m_data = data;
    }

    public String getDailyMenu()
    {
        return m_result;
    }

    public abstract boolean Parse();

    protected String m_data;
    protected String m_result;
}
