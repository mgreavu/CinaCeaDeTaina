package com.gorunului.mig.cinaceadetaina;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.io.IOException;

import android.util.Log;

public class MainActivity extends AppCompatActivity
{
    TextView m_txtLunchData;
    TextView m_txtLog;
    String[] m_urls = { "http://www.la-cina.ro/meniul_zilei.php" };
    String[] m_results;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnReload = (Button)findViewById(R.id.btnReload);
        m_txtLunchData = (TextView)findViewById(R.id.txtLunchData);
        m_txtLunchData.setMovementMethod(new ScrollingMovementMethod());

        m_txtLog = (TextView)findViewById(R.id.txtLog);

        btnReload.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                m_txtLunchData.setText("");
                LunchFetcherTask fetcher = new LunchFetcherTask();
                fetcher.execute(m_urls);
            }
        });

        Log.d("INFO", "onCreate() --> The activity is being created");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("INFO", "onStart() --> The activity is about to become visible");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("INFO", "onResume() --> The activity has become visible (it is now \"resumed\")");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d("INFO", "onPause() --> Another activity is taking focus (this activity is about to be \"paused\")");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d("INFO", "onStop() --> The activity is no longer visible (it is now \"stopped\")");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("INFO", "onDestroy() --> The activity is about to be destroyed");
    }

    private class LunchFetcherTask extends AsyncTask<String, String, Long>
    {
        protected void onPreExecute()
        {
            m_txtLog.setText("Descarcare meniu in desfasurare...");
        }

        protected Long doInBackground(String... urls)
        {
            HttpURLConnection urlConnection;
            long result = 0L;

            try
            {
                int count = urls.length;
                m_results = new String[count];

                for (int i = 0; i < count; i++)
                {
                    StringBuffer sbuff = new StringBuffer(4096);

                    URL url = new URL(urls[i]);
                    urlConnection = (HttpURLConnection)url.openConnection();

                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    String line;
                    while ((line = reader.readLine()) != null)
                    {
                        sbuff.append( line );
                    }

                    in.close();
                    urlConnection.disconnect();

                    // create parser
                    LunchParser parser = new LaCinaParser(sbuff.toString());
                    parser.Parse();

                    m_results[i] = parser.getDailyMenu();
                    publishProgress(m_results[i]);

                    // Escape early if cancel() is called
                    if (isCancelled())
                    {
                        break;
                    }
                }
            }
            catch (MalformedURLException e)
            {
                Log.w("WARN", "doInBackground() --> " + e.toString());
                result = -1L;
            }
            catch (IOException e)
            {
                Log.w("WARN", "doInBackground() --> " + e.toString());
                result = -1L;
            }
            return result;
        }

        /*
            onProgressUpdate(Progress...), invoked on the UI thread after a call to publishProgress(Progress...).
            The timing of the execution is undefined.
        */
        protected void onProgressUpdate(String... progress)
        {
            m_txtLunchData.append(System.lineSeparator());
            if (progress[0] == null)
            {
                m_txtLunchData.append("Error while parsing received data");
                return;
            }
            m_txtLunchData.append(progress[0]);
        }

        protected void onPostExecute(Long result)
        {
            if (result == -1L)
            {
                m_txtLog.append("fara succes.");
            }
            else
            {
                m_txtLog.append("succes.");
            }
        }
    }
}
